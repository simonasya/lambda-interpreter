from interpreter.expression import Expression
from interpreter.variable import Variable
from interpreter.lambda_element import Lambda
from interpreter_tests.colors import GREEN, END


def test_expression_representation():
    print('Test expression representation: ', end='')

    expression_0 = Expression([
        Lambda(['x'], Expression([Variable('x'), Variable('y')])),
        Variable(8)
    ])

    expression_1 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([Variable('y'), Variable('x')])),
            Variable('x'),
            Variable('y')
        ])),
        Variable('z')
    ])

    expression_2 = Expression([
        Variable('x'),
        Variable('y'),
        Variable('z'),
        Variable('a'),
        Variable('b')
    ])

    expression_3 = Expression([
        Variable('a'),
        Lambda(['b'], Expression([
            Variable('x'),
            Lambda(['c'], Expression([
                Variable('y'), Variable('t'),
                Lambda(['y', 'x', 'c'], Expression([
                    Variable(12)
                ]))
            ]))
        ])),
        Variable(13),
        Variable('p'),
        Lambda(['x'], Expression([
            Variable(2), Variable(3)
        ]))
    ])

    assert expression_0.representation() == '((lambda x. (x y)) 8)'
    assert expression_1.representation() == '((lambda x. ((lambda y. (y x)) x y)) z)'
    assert expression_2.representation() == '(x y z a b)'
    assert expression_3.representation() == '(a (lambda b. (x (lambda c. (y t (lambda yxc. 12))))) 13 p (lambda x. (2 3)))'
    print(GREEN + 'Passed' + END)


def test_expression_variable_detection():
    print('Test expression variable detection: ', end='')

    # (λ. x  (λy. y x) x y) z from edux
    expression_0 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([Variable('y'), Variable('x')])),
            Variable('x'),
            Variable('y')
        ])),
        Variable('z')
    ])

    expression_1 = Expression([
        Variable('z'),
        Variable('z'),
        Variable('z'),
        Lambda(['y'], Expression([Variable('y'), Variable('x')]))
    ])

    lambda_0 = Lambda(['x', 'y'], Expression([Variable('y'), Variable('x')]))

    lambda_1 = Lambda(['y'], Expression([
        Lambda(['y'], Expression([Variable('y'), Variable('x')]))
    ]))

    expression_3 = Expression([
        lambda_0,
        lambda_1
    ])

    lambda_2 = Lambda(['x', 'y'], Expression([Variable('z'), Variable('z')]))

    assert expression_0.detect_variables() == [['y', 'bound'], ['x', 'bound'], ['x', 'bound'], ['y', 'free'], ['z', 'free']]
    assert expression_1.detect_variables() == [['z', 'free'], ['z', 'free'], ['z', 'free'], ['y', 'bound'], ['x', 'free']]
    assert lambda_0.detect_variables() == [['y', 'bound'], ['x', 'bound']]
    assert lambda_1.detect_variables() == [['y', 'bound'], ['x', 'free']]
    assert expression_3.detect_variables() == [['y', 'bound'], ['x', 'bound'], ['y', 'bound'], ['x', 'free']]
    assert lambda_2.detect_variables() == [['z', 'free'], ['z', 'free']]

    print(GREEN + 'Passed' + END)

