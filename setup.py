from setuptools import setup

setup(
    name='lambda_interpreter',
    version='1.0',
    packages=['interpreter'],
    url='https://bitbucket.org/simonasya/lambda-interpreter',
    license='GNU General Public License v3.0',
    author='Simona Kurnavova',
    author_email='simasya.k@gmail.com',
    description='This projects aim is to evaluate lambda calculus expressions.',
    install_requires=[]
)
