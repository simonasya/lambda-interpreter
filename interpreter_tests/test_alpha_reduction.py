from interpreter.expression import Expression
from interpreter.lambda_element import Lambda
from interpreter.variable import Variable
from interpreter_tests.colors import GREEN, END


def test_alpha_reduction():
    print('Test alpha reduction: ', end='')

    # (lambda xy. x y) y -> (lambda xt. x t) y
    expression_1 = Expression([
        Lambda(['x', 'y'], Expression([
            Variable('x'), Variable('y')
        ])),
        Variable('y')
    ])

    # (lambda x. (lambda z. x z)) (z g) -> (lambda x. (lambda t. x t)) (z g)
    expression_2 = Expression([
        Lambda(['x'],
               Lambda(['z'], Expression([
                   Variable('x'), Variable('z')
               ]))),
        Expression([
            Variable('z'), Variable('g')
        ])
    ])

    # (lambda y. (lambda z. z y) y z) - no change
    expression_3 = Expression([
        Lambda(['y'], Expression([
            Lambda(['z'], Expression([
                Variable('z'),
                Variable('y')
            ]))
        ])),
        Variable('y'),
        Variable('z')
    ])

    expression_4 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Variable('y'),
                Lambda(['x'], Variable('x')),
                Variable('x')
            ]))
        ])),
        Variable('x')
    ])

    assert expression_1.representation() == '((lambda xy. (x y)) y)'
    expression_1.normal_evaluation()
    assert expression_1.representation() == "(lambda y'. (y y'))"

    assert expression_2.representation() == '((lambda x. (lambda z. (x z))) (z g))'
    expression_2.normal_evaluation()
    assert expression_2.representation() == "(lambda z'. ((z g) z'))"

    assert expression_3.representation() == '((lambda y. (lambda z. (z y))) y z)'
    expression_3.applicative_evaluation()
    assert expression_3.representation() == '(z y)'

    assert expression_4.representation() == '((lambda x. (lambda y. (y (lambda x. x) x))) x)'
    expression_4.applicative_evaluation()
    assert expression_4.representation() == "(lambda y. (y (lambda x'. x') x))"

    print(GREEN + 'Passed' + END)

