## Lambda calculus interpreter library

This projects aim is to evaluate lambda calculus expressions.
All the elements of lambda calculus are represented as python classes.

It allows minimalization of given expression in either applicative or normal way, as defines lambda calculus.
It has also implementation of alpha reduction to detect naming conflicts.

## Running tests:

from interpreter_tests.[name of the test file] import [name of the test function]        
[name of the test function] ()

## For running all of the tests:

from interpreter_tests.all_tests import run     
run()